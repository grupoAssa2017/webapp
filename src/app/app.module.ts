import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";

//Componentes
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { ListDetailComponent } from './list-detail/list-detail.component';

//Https
import { HttpClientModule } from '@angular/common/http';

//Providers
import { SqldbService } from "../services/sqldb/sqldb.service";
import { GoogleVisionService } from "../services/googleVision/google-vision.service";
import { googleCloudVisionAPIKey } from '../environments/environment';



@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ListDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: ListComponent },
      { path: 'list-detail/:imageId', component: ListDetailComponent }
    ])
  ],
  providers: [
    SqldbService,
    GoogleVisionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
