import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';

//Interfaz
import { baseInfo, FIELDS } from "../../environments/environment";

//Servicio
import { SqldbService } from '../../services/sqldb/sqldb.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  vg_persons: baseInfo[];
  vg_person: any;
  vg_show: true;
  vg_notif_text: string;
  fields: any;

  constructor(
    private sql: SqldbService
  ) { }

  ngOnInit() {
    //Inicializar Variables
    this.fields = FIELDS;
    this.vg_person = new Object();
    this.vg_persons = [];
    //Modals Inicales
    this.showModal('Cargando....');
    this.sql.getAll().subscribe(
      (data: any) => {
        this.fillPersons(data);
        this.showModal('Informacion cargada correctamente');
        this.hideModal();
      }, err => {
        this.showModal('Error al cargar informacion');
        this.hideModal();
      }
    );
  }

  fillPersons(data: any) {
    var keys = Object.keys(data);
    for (var i = 0; i < keys.length; i++) {
      var k = keys[i];
      var body = data[k].body;
      
      this.vg_person.imageId = k;
      this.vg_person.base64Image = body['base64Image'];
      this.vg_person.capturedLastName = body['capturedLastName'];
      this.vg_person.visionLastName = body['visionLastName'];
      this.vg_person.isSameLastName = body['isSameLastName'];
      this.vg_person.capturedMotherLastName = body['capturedMotherLastName'];
      this.vg_person.visionMotherLastName = body['visionMotherLastName'];
      this.vg_person.isSameMotherLastName = body['isSameMotherLastName'];
      this.vg_person.capturedFirstName = body['capturedFirstName'];
      this.vg_person.visionFirstName = body['visionFirstName'];
      this.vg_person.isSameFirstName = body['isSameFirstName'];
      this.vg_person.capturedSecondName = body['capturedSecondName'];
      this.vg_person.visionSecondName = body['visionSecondName'];
      this.vg_person.isSameSecondName = body['isSameSecondName'];
      this.vg_person.capturedStreet = body['capturedStreet'];
      this.vg_person.visionStreet = body['visionStreet'];
      this.vg_person.isSameStreet = body['isSameStreet'];
      this.vg_person.capturedOutdoorNumber = body['capturedOutdoorNumber'];
      this.vg_person.visionOutdoorNumber = body['visionOutdoorNumber'];
      this.vg_person.isSameOutdoorNumber = body['isSameOutdoorNumber'];
      this.vg_person.capturedIndorNumber = body['capturedIndorNumber'];
      this.vg_person.visionIndorNumber = body['visionIndorNumber'];
      this.vg_person.isSameIndorNumber = body['isSameIndorNumber'];
      this.vg_person.capturedPostalCode = body['capturedPostalCode'];
      this.vg_person.visionPostalCode = body['visionPostalCode'];
      this.vg_person.isSamePostalCode = body['isSamePostalCode'];
      this.vg_person.capturedSuburb = body['capturedSuburb'];
      this.vg_person.visionSuburb = body['visionSuburb'];
      this.vg_person.isSameSuburb = body['isSameSuburb'];
      this.vg_person.capturedMunicipality = body['capturedMunicipality'];
      this.vg_person.visionMunicipality = body['visionMunicipality'];
      this.vg_person.isSameMunicipality = body['isSameMunicipality'];
      this.vg_person.capturedCity = body['capturedCity'];
      this.vg_person.visionCity = body['visionCity'];
      this.vg_person.isSameCity = body['isSameCity'];
      this.vg_person.capturedState = body['capturedState'];
      this.vg_person.visionState = body['visionState'];
      this.vg_person.isSameState = body['isSameState'];

      this.vg_persons.push(this.vg_person);
      this.vg_person = new Object();
    }
  }

  showModal(text: string) {
    this.vg_notif_text = text;
    document.getElementById('id01').style.display = "block";
  }
  hideModal() {
    setTimeout(() => {
      document.getElementById('id01').style.display = "none";
    }, 2000);
  }
}
