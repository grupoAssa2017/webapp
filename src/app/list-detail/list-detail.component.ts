import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SqldbService } from "../../services/sqldb/sqldb.service";
import { GoogleVisionService } from "../../services/googleVision/google-vision.service";
import { baseInfo, htmlStruct, FIELDS } from "../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';
import { SafeUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
import { transition } from '@angular/core/src/animation/dsl';

@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.css']
})
export class ListDetailComponent implements OnInit {

  person: any;
  capturedArray: htmlStruct[];
  visionArray: htmlStruct[];
  compareArray: htmlStruct[];
  generalArray: htmlStruct[];
  htmlStruct: htmlStruct;
  fields: any;
  visionLastName: any;
  visionMotherLastName: any;
  visionFirstName: any;
  visionSecondName: any;
  visionStreet: any;
  visionOutdoorNumber: any;
  visionPostalCode: any;
  visionSuburb: any;
  visionMunicipality: any;
  visionState: any;
  visionObject: any;
  visionFields: any;
  key: any;
  flag_id: any;
  vg_notif_text: string;
  vg_image: string;
  vg_image_to_show: SafeUrl;
  vg_full_name: string;
  vg_result: any;
  vg_label_annotations: any[];
  vg_images_array: any[];
  vg_visionArr: any[];
  showVisionLastName: any;
  vg_dataVisionArr: any[];

  constructor(
    private route: ActivatedRoute,
    private sql: SqldbService,
    private vision: GoogleVisionService,
    private sanit: DomSanitizer
  ) { }

  ngOnInit() {
    //Inicializar
    this.flag_id = true;
    this.capturedArray = [];
    this.compareArray = [];
    this.visionArray = [];
    this.generalArray = [];
    this.vg_images_array = [];
    this.fields = FIELDS;
    this.person = [];
    this.vg_image_to_show = '';
    this.vg_full_name = '';
    this.vg_visionArr = [];
    this.vg_dataVisionArr = [];

    this.getData();
  };
  getData() {
    //Servicio

    this.showModal("Cargando....");
    this.route.paramMap.subscribe(params => {
      let id = params.get('imageId');
      this.key = params.get('imageId');
      this.sql.getAll().subscribe(
        (data: any) => {
          this.showModal("Informacion de BD OK");
          this.person = data[id].body;
          this.person.imageId = id;
          console.log(this.person);
          this.callGoogle();
        }, err => {
          this.showModal("Error al cargar la informacion: " + err);
          this.hideModal();
        }
      );
    }
    )
  }
  callGoogle() {
    //Google Vision
    this.flag_id = false;
    this.vision.getImageInfo(this.person.base64Image).subscribe((result) => {
      console.log(result);
      this.vg_result = result;
      this.vg_label_annotations = this.vg_result.responses[0].labelAnnotations;
      for (let index = 0; index < this.vg_label_annotations.length; index++) {
        const element = this.vg_label_annotations[index];
        if (element.mid == '/m/01_v7j' && element.score > '0.80') {
          this.hideModal();
          this.showModal("Se detectó identificacion");
           this.flag_id = true;
      //     this.cropImage();
         this.mapInfo();
         //this.callGoogleMap();
      //     this.hideModal();
        }
      }
      if (!this.flag_id) {
        this.showModal("No se detectó identificacion correcta");
        this.mapInfo();
        this.hideModal();
      }
    }, err => {
      this.showModal("Error al cargar Vision");
      this.hideModal();
    }
    );
  }

  callGoogleMap(){
    this.showModal("Obteniendo información de la identificacion");
    this.vision.getImage(this.person.base64Image).subscribe((result) => {
      console.log(result);
      this.vg_result = result;
      
      
    /* PROBANDO MATCH */
      let tryMatch = this.vg_result.responses[0].textAnnotations;
      let stopName = false;
      let stopAddress = false;
      let extNumbReady = false;
      let intNumbReady = false;
      let postalCodeReady = false;
      let intNumbYPosition = 0;
      let municipalityReady = false;
      
      for (let i = 0; i < tryMatch.length; i++) {
        let tryMatchDescription = tryMatch[i].description;
        console.log(tryMatchDescription)
        if (tryMatchDescription === 'NOMBRE') {
          for (let j = i + 1; (j < tryMatch.length) && !stopName ; j++){
            let tryMatchName = tryMatch[j].description;
            if(tryMatchName === 'DOMICILIO'|| tryMatchName === 'DOMICILIC'){
              stopName = true;
              this.person.visionLastName = tryMatch[i+1].description
              this.person.visionMotherLastName = tryMatch[i+2].description
              this.person.visionFirstName = tryMatch[i+3].description
              if(j>(i+4)){
                this.person.visionSecondName = tryMatch[i+4].description
              }

              this.person.visionStreet = ""
              this.person.visionSuburb = ""
              for(let k = j + 1; (k < tryMatch.length) && !stopAddress ; k++){
                tryMatchName = tryMatch[k].description;
                if(tryMatchName === 'CLAVE' || tryMatchName === 'FOLIO' || tryMatchName === 'FOUO'){
                  stopAddress = true;
                }

                if(!isNaN(Number(tryMatch[k].description))){
                  if(tryMatch[k].description.length==5){
                    postalCodeReady = true;
                    this.person.visionPostalCode = tryMatch[k].description
                  }else if(!extNumbReady){
                    this.person.visionOutdoorNumber = tryMatch[k].description
                    extNumbReady=true
                  }else if(!intNumbReady){
                    this.person.visionIndorNumber = tryMatch[k].description
                    intNumbReady=true
                    intNumbYPosition=tryMatch[k].boundingPoly.vertices[3].y
                  }
                }else if(!extNumbReady && tryMatch[k].description!="MZ"){
                  this.person.visionStreet += tryMatch[k].description + " "
                }else if(extNumbReady&&intNumbReady&&!postalCodeReady && (tryMatch[k].boundingPoly.vertices[3].y-intNumbYPosition>1)){
                  this.person.visionSuburb += tryMatch[k].description + " "
                }else if(postalCodeReady&&!municipalityReady){
                  municipalityReady = true;
                  this.person.visionMunicipality = tryMatch[k].description.replace(',','').replace(/\./g,'')
                }else if(municipalityReady&&!stopAddress){
                  this.person.visionState = tryMatch[k].description.replace(',','').replace(/\./g,'')
                }

                i = k
            }
            this.person.visionStreet = this.person.visionStreet.substring(0, this.person.visionStreet.length - 1)
            this.person.visionSuburb = this.person.visionSuburb.substring(0, this.person.visionSuburb.length - 1)
          }
        }
      }
    }
      this.visionMap()
      this.postVisionDB()
    /* PROBANDO MATCH */
    }, err => {
      this.showModal("Error al cargar Vision");
      this.hideModal();
    })
    
  }

  callGoogleArray() {
    var imgArr = [];
    var base64Arr = [];

    var canvas = <HTMLCanvasElement>document.getElementById('idDocument');
    var ctx = canvas.getContext('2d');
    imgArr.push(ctx);
    var canvas = <HTMLCanvasElement>document.getElementById('name');
    var ctx2 = canvas.getContext('2d');
    imgArr.push(ctx2);
    var canvas = <HTMLCanvasElement>document.getElementById('address');
    var ctx3 = canvas.getContext("2d");
    imgArr.push(ctx3);
    var canvas = <HTMLCanvasElement>document.getElementById('electoral');
    var ctx4 = canvas.getContext('2d')
    imgArr.push(ctx4);
    var canvas = <HTMLCanvasElement>document.getElementById('birthday');
    var ctx5 = canvas.getContext('2d');
    imgArr.push(ctx5);

    for (let index = 0; index < imgArr.length; index++) {
      const element = imgArr[index];
      var dataURL = element.canvas.toDataURL();
      base64Arr.push(dataURL.replace('data:image/png;base64,', ''));
    }
    this.vision.getAllImages(base64Arr).subscribe((result) => {
      console.log(result);

      this.visionFields = result;
      this.vg_dataVisionArr = this.visionFields.responses;
        for (let i = 0; i < this.vg_dataVisionArr.length; i++) {
          let textAnnotations = this.vg_dataVisionArr[i].textAnnotations;
          for (let j=0; j < textAnnotations.length; j++ ) {
            let description = textAnnotations[j].description;

            if (description === 'NOMBRE'){
              this.person.visionLastName = textAnnotations[j].description;
              this.person.visionMotherLastName = textAnnotations[j + 1].description;
              this.person.visionFirstName = textAnnotations[4].description;
              //this.person.vg_visionArr.push(this.person.visionLastName, this.person.visionMotherLastName, this.person.visionFirstName);
              }
           // var flagStreet = false;
            if (description === 'DOMICILIC' || description === 'DOMICILIO' ){
           //    for(var ind = 0; ind < textAnnotations.length ; ind++){
           //      if(flagStreet === false){
           //        this.person.visionStreet = this.person.visionStreet + " " + textAnnotations[ind].description;
           //      }
           //      if(isNaN(textAnnotations[ind])){
           //        flagStreet = true;
           //      }
           //    }
              this.person.visionStreet = textAnnotations[2].description + " " + textAnnotations[3].description + " " + textAnnotations[4].description;
              this.person.visionOutdoorNumber = textAnnotations[6].description;
              this.person.visionIndorNumber = textAnnotations[8].description;
              this.person.visionPostalCode = textAnnotations[15].description;
              this.person.visionSuburb = textAnnotations[11].description + " " + textAnnotations[12].description + " " + textAnnotations[13].description + " " + textAnnotations[14].description;
              this.person.visionMunicipality = textAnnotations[16].description;
              this.person.visionState = textAnnotations[17].description;
            }
          }
        }
        this.hideModal();
        this.visionMap()
        this.postVisionDB()
    }, err => {
      this.showModal("Error al Obtener Vision");
      this.hideModal()
    })
  }

  visionMap(){
    if (this.person.capturedLastName == this.person.visionLastName) {
    this.person.isSameLastName = true
    } else {
      this.person.isSameLastName = false
    }
    if (this.person.capturedMotherLastName == this.person.visionMotherLastName) {
        this.person.isSameMotherLastName = true
    } else {
      this.person.isSameMotherLastName = false
    }
    if (this.person.capturedFirstName == this.person.visionFirstName) {
        this.person.isSameFirstName = true
    } else {
      this.person.isSameFirstName = false
    }
    if (this.person.capturedSecondName == this.person.visionSecondName) {
        this.person.isSameSecondName = true
    } else {
      this.person.isSameSecondName = false
    }
    if (this.person.capturedStreet == this.person.visionStreet) {
        this.person.isSameStreet = true
    } else {
      this.person.isSameStreet = false
    }
    if (this.person.capturedOutdoorNumber == this.person.visionOutdoorNumber) {
        this.person.isSameOutdoorNumber = true
    } else {
      this.person.isSameOutdoorNumber = false
    }
    if (this.person.capturedIndorNumber == this.person.visionIndorNumber) {
        this.person.isSameIndorNumber = true
    } else {
      this.person.isSameIndorNumber = false
    }
    if (this.person.capturedPostalCode == this.person.visionPostalCode) {
        this.person.isSamePostalCode = true
    } else {
      this.person.isSamePostalCode = false
    }
    if (this.person.capturedSuburb == this.person.visionSuburb) {
        this.person.isSameSuburb = true
    } else {
      this.person.isSameSuburb = false
    }
    if (this.person.capturedMunicipality == this.person.visionMunicipality) {
        this.person.isSameMunicipality = true
    } else {
      this.person.isSameMunicipality = false
    }
    if (this.person.capturedCity == this.person.visionCity) {
        this.person.isSameCity = true
    } else {
      this.person.isSameCity = false
    }
    if (this.person.capturedState == this.person.visionState) {
        this.person.isSameState = true
    } else {
      this.person.isSameState = false
    }

  }

  postVisionDB(){
    
      console.log(this.person);

      this.sql.updateInfo(this.person).subscribe((data) => {
        console.log('Datos:' , data);
        location.reload();
      },err =>{
        console.error("error",err);
      });
  }

  cropImage() {
    var srcOrig = 'data:image/png;base64,' + this.person['base64Image'];
    var imgArr = [];
    var img = new Image(500,300);
    var visionArr = [];

    //LLenado de arreglo
    var canvas = <HTMLCanvasElement>document.getElementById('idDocument');
    var ctx = canvas.getContext('2d');
    imgArr.push(ctx);
    var canvas = <HTMLCanvasElement>document.getElementById('name');
    var ctx2 = canvas.getContext('2d');
    imgArr.push(ctx2);
    var canvas = <HTMLCanvasElement>document.getElementById('address');
    var ctx3 = canvas.getContext("2d");
    imgArr.push(ctx3);
    var canvas = <HTMLCanvasElement>document.getElementById('electoral');
    var ctx4 = canvas.getContext('2d')
    imgArr.push(ctx4);
    var canvas = <HTMLCanvasElement>document.getElementById('birthday');
    var ctx5 = canvas.getContext('2d');
    imgArr.push(ctx5);

    canvas.width = 500;
    canvas.height = 300;
    img.onload = function () {
      imgArr[0].drawImage(img, 10, 0, img.naturalWidth, img.naturalHeight, 10, 10, canvas.width, canvas.height);
      imgArr[1].drawImage(img, 270, 180, img.naturalWidth, img.naturalHeight, 10, 10, canvas.width, canvas.height);
      imgArr[2].drawImage(img, 270, 315, img.naturalWidth, img.naturalHeight, 10, 10, canvas.width, canvas.height);
      imgArr[3].drawImage(img, 270, 450, img.naturalWidth, img.naturalHeight, 10, 10, canvas.width, canvas.height);
      imgArr[4].drawImage(img, 730, 125, img.naturalWidth, img.naturalHeight, 10, 10, canvas.width, canvas.height);
    }
    img.src = srcOrig;
  }
  mapInfo() {
    var flag: number;
    for (var i = 0; i < this.fields.length; i++) {
      switch (this.fields[i].name) {
        case 'imageId':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 0;
          break;
        case 'base64Image':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 0;

          this.vg_image = 'data:image/png;base64, ' + this.person[this.fields[i].name];
          this.vg_image_to_show = this.sanit.bypassSecurityTrustUrl(this.vg_image);
          break;
        case 'capturedLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          this.vg_full_name = this.vg_full_name + ' ' + this.person[this.fields[i].name];
          flag = 1;
          break;
        case 'visionLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedMotherLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          this.vg_full_name = this.vg_full_name + ' ' + this.person[this.fields[i].name];
          flag = 1;
          break;
        case 'visionMotherLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameMotherLastName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedFirstName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          this.vg_full_name = this.vg_full_name + ' ' + this.person[this.fields[i].name];
          flag = 1;
          break;
        case 'visionFirstName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameFirstName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedSecondName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          //this.vg_full_name = this.vg_full_name + ' ' + this.person[this.fields[i].name];
          flag = 1;
          break;
        case 'visionSecondName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameSecondName':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedStreet':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionStreet':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameStreet':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedOutdoorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionOutdoorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameOutdoorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedIndorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionIndorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameIndorNumber':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedPostalCode':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionPostalCode':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSamePostalCode':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedSuburb':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionSuburb':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameSuburb':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedMunicipality':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionMunicipality':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameMunicipality':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedCity':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionCity':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameCity':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        case 'capturedState':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 1;
          break;
        case 'visionState':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 2;
          break;
        case 'isSameState':
          this.htmlStruct = { metaData: this.fields[i].name, value: this.person[this.fields[i].name] }
          flag = 3;
          break;
        default:
          break;
      }
      switch (flag) {
        case 0:
          this.generalArray.push(this.htmlStruct);
          break;
        case 1:
          this.capturedArray.push(this.htmlStruct);
          break;
        case 2:
          this.visionArray.push(this.htmlStruct);
          break;
        case 3:
          this.compareArray.push(this.htmlStruct);
          break;

        default:
          break;
      }
    }
  }
  showModal(text: string) {
    this.vg_notif_text = text;
    document.getElementById('id01').style.display = "block";
  }
  hideModal() {
    setTimeout(() => {
      document.getElementById('id01').style.display = "none";
    }, 4000);
  }
}
