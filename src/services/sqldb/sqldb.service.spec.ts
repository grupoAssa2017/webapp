import { TestBed, inject } from '@angular/core/testing';

import { SqldbService } from './sqldb.service';

describe('SqldbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SqldbService]
    });
  });

  it('should be created', inject([SqldbService], (service: SqldbService) => {
    expect(service).toBeTruthy();
  }));
});
