import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class SqldbService {

  visionObject: any;
visionLastName: any;
visionMotherLastName: any;
visionFirstName: any;
visionSecondName: any;
visionStreet: any;
visionOutdoorNumber: any;
person: any;
visionPostalCode: any;
visionSuburb: any;
visionMunicipality: any;
visionState: any;
data: any;

  httpHeaders = new HttpHeaders();

  constructor(public httpClient: HttpClient) {
    this.httpHeaders.append('Access', 'application/json');
    this.httpHeaders.append('Content-Type', 'application/json');

    this.httpHeaders.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    this.httpHeaders.append('Access-Control-Allow-Credentials', 'true');
  }



  ngOnInit() {
  }

  getAll() {

    return this.httpClient.get(
      "https://poc-ai.herokuapp.com/get"
      // "http://localhost:5000/get"
    );
  };

  updateInfo(visionObject){
    var body = visionObject;
    console.log(body)

    return this.httpClient.post(
      "https://poc-ai.herokuapp.com/update" , 
      body,
      {
        headers: this.httpHeaders
      });
  }
}
