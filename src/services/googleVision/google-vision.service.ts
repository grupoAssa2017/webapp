import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { googleCloudVisionAPIKey } from "../../environments/environment";
import { ListDetailComponent } from '../../../src/app/list-detail/list-detail.component';
import 'rxjs/add/operator/map';

@Injectable()
export class GoogleVisionService {

visionObject: any;
visionLastName: any;
visionMotherLastName: any;
visionFirstName: any;
visionSecondName: any;
visionStreet: any;
visionOutdoorNumber: any;
visionPostalCode: any;
visionSuburb: any;
visionMunicipality: any;
visionState: any;
data: any;

  httpHeaders = new HttpHeaders();

  constructor(public http: HttpClient) {
    this.httpHeaders.append('Access', 'application/json');
    this.httpHeaders.append('Content-Type', 'application/json');

    this.httpHeaders.append('Access-Control-Allow-Origin', '*');
    this.httpHeaders.append('Access-Control-Allow-Credentials', 'true');
  }

  getImageInfo(base64Image) {
    const body = {
      "requests": [
        {
          "image": {
            "content": base64Image
          },
          "features": [
            {
              "type": "LABEL_DETECTION"
            }
          ]
        }
      ]
    }
    return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + googleCloudVisionAPIKey, body);
  };

  getImage(base64Image) {
    const body = {
      "requests": [
        {
          "image": {
            "content": base64Image
          },
          "features": [
            {
              "type": "TEXT_DETECTION"
            }
          ]
        }
      ]
    }
    return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + googleCloudVisionAPIKey, body);
  };

  getAllImages(images) {
    var body: any;
    body =
      {
        "requests": []
      }
    for (var i = 0; i < images.length; i++) {
      var request =
        {
          "image": {
            "content": images[i]
          },
          "features": [
            {
              "type": "TEXT_DETECTION"
            }
          ]
        }
      body.requests.push(request);
    }
    return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + googleCloudVisionAPIKey, body);
  }

  
}