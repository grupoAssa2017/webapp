import { toBase64String } from "@angular/compiler/src/output/source_map";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};

export const googleCloudVisionAPIKey = "AIzaSyAawhntwB_XFnROyTgZgXNhX1kwd-xoE9U";

export interface baseInfo {
  imageId: string,
  base64Image: string,
  capturedLastName: string,
  visionLastName: string,
  isSameLastName: boolean,
  capturedMotherLastName: string,
  visionMotherLastName: string,
  isSameMotherLastName: boolean,
  capturedFirstName: string,
  visionFirstName: string,
  isSameFirstName: boolean,
  capturedSecondName: string,
  visionSecondName: string,
  isSameSecondName: boolean,
  capturedStreet: string,
  visionStreet: string,
  isSameStreet: boolean,
  capturedOutdoorNumber: string,
  visionOutdoorNumber: string,
  isSameOutdoorNumber: boolean,
  capturedIndorNumber: string,
  visionIndorNumber: string,
  isSameIndorNumber: boolean,
  capturedPostalCode: string,
  visionPostalCode: string,
  isSamePostalCode: boolean,
  capturedSuburb: string,
  visionSuburb: string,
  isSameSuburb: boolean,
  capturedMunicipality: string,
  visionMunicipality: string,
  isSameMunicipality: boolean,
  capturedCity: string,
  visionCity: string,
  isSameCity: boolean,
  capturedState: string,
  visionState: string,
  isSameState: boolean
};

export interface htmlStruct {
  metaData: string,
  value: any
};

export const FIELDS = [
  { "name": "imageId" },
  { "name": "base64Image" },
  { "name": "capturedLastName" },
  { "name": "visionLastName" },
  { "name": "isSameLastName" },
  { "name": "capturedMotherLastName" },
  { "name": "visionMotherLastName" },
  { "name": "isSameMotherLastName" },
  { "name": "capturedFirstName" },
  { "name": "visionFirstName" },
  { "name": "isSameFirstName" },
  { "name": "capturedSecondName" },
  { "name": "visionSecondName" },
  { "name": "isSameSecondName" },
  { "name": "capturedStreet" },
  { "name": "visionStreet" },
  { "name": "isSameStreet" },
  { "name": "capturedOutdoorNumber" },
  { "name": "visionOutdoorNumber" },
  { "name": "isSameOutdoorNumber" },
  { "name": "capturedIndorNumber" },
  { "name": "visionIndorNumber" },
  { "name": "isSameIndorNumber" },
  { "name": "capturedPostalCode" },
  { "name": "visionPostalCode" },
  { "name": "isSamePostalCode" },
  { "name": "capturedSuburb" },
  { "name": "visionSuburb" },
  { "name": "isSameSuburb" },
  { "name": "capturedMunicipality" },
  { "name": "visionMunicipality" },
  { "name": "isSameMunicipality" },
  { "name": "capturedCity" },
  { "name": "visionCity" },
  { "name": "isSameCity" },
  { "name": "capturedState" },
  { "name": "visionState" },
  { "name": "isSameState" }
]